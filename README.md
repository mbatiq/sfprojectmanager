project_manager (symfony 2.8)
===================

A Symfony project created on October 25, 2017, 2:26 pm.

URUCHOMIENIE:

1. Sklonuj repozytorium (zgodnie z dokumentacją)
	- cd projects/
	- git clone ...
	- cd my_project_name/
	- composer install
	- ustaw to o co poprosi composer
	
2. Wykonaj polecenia
	- php app/console doctrine:schema:update
	- php app/console doctrine:schema:update --force

3. Dodaj użytkownika (max 10, bo takie testowe dane) przez konsolę: 
	php app/console fos:user:create
	lub z poziomu aplikacji

4. W pliku wypelnianieSQL/wypelnij.php ustaw:
	- nazwę bazy danych
	- użytkownika
	- hasło

5. Uruchom w konsoli plik wypelnianieSQL/wypelnij.php:
	- php wypelnij.php
