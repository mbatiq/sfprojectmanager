<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Task;

class taskController extends Controller
{
    /**
     * @Route("/task", name="task_list")
     */
    public function indexAction(Request $request)
    {

        $pagination = null;

        // Jeśli użytkownik jest zalogowany to widzi wszystkie swoje zadania
        if ($user = $this->getUser()) {

            $tasks = $this->getDoctrine()
                ->getRepository('AppBundle:Task');

            // Zadania posortowanie po statusie i po nazwie
            $query = $tasks->createQueryBuilder('t')
                ->where('t.user = :user_id')
                ->setParameter('user_id', $user->getId())
                ->addOrderBy('t.status', 'ASC')
                ->addOrderBy('t.name', 'ASC')
                ->getQuery()
                ->getResult();

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $query,
                $request->query->get('page', 1),
                10
            );
        }

        return $this->render('task/task-list.html.twig', array(
            'tasks' => $pagination,
        ));

    }


    /**
     * @Route("/task/{id}", name="task_show")
     */

    public function showAction($id)
    {
        $task = null;
        $projectId = null;
        $projectName = null;

        $user_first_name = '';
        $user_last_name = '';

        if ($user = $this->getUser()) {

            $task = $this->getDoctrine()
                ->getRepository('AppBundle:Task')
                ->find($id);

            if (!$task){
                throw $this->createNotFoundException(
                    'Task '.$id.' does not exist'
                );
            }

            $projectId = $task->getProject()->getId();
            $projectName = $task->getProject()->getName();

            if ($task->getUser()) {
                $user_first_name = $task->getUser()->getFirstName();
                $user_last_name = $task->getUser()->getLastName();
            }

        };

        return $this->render('task/task-show.html.twig', array(
            'task' => $task,
            'project_id' => $projectId,
            'project_name' => $projectName,
            'first_name' => $user_first_name,
            'last_name' => $user_last_name
        ));
    }
}