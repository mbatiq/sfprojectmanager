<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class projectController extends Controller
{
    /**
     * @Route("/", name="project_list")
     */
    public function listAction(Request $request)
    {
        $pagination = null;

        // Jeśli użytkownik jest zalogowany
        if ($user = $this->getUser()) {

            $projects_repository = $this->getDoctrine()
                ->getRepository('AppBundle:Project');

            $task_repository = $this->getDoctrine()
                ->getRepository('AppBundle:Task');

            $projects = $projects_repository
                ->findAll();

            // Wyliczenie procenta realizacji projektów
            foreach ($projects as $project)
            {
                // Wszystkie taski z danego projektu
                $project_task = $project->getTasks();
                // Ilość wszystkich tasków z danego projektu
                $number_tasks = count($project_task);

                // Ukończone taski
                $completed_tasks = $task_repository->createQueryBuilder('t')
                    ->where('t.status = :status_name')
                    ->setParameter('status_name', 'zakonczone')
                    ->andWhere('t.project = :project_id')
                    ->setParameter('project_id', $project->getId())
                    ->getQuery()
                    ->getResult();

                // Ilość ukończonych tasków
                $completed = count($completed_tasks);

                // Jak są taski w projekcie, to policz i ustaw procent ukończenia projektu,
                // jak nie ma to zostaw domyślną wartość: 0
                if (!$number_tasks == 0)
                {
                    $completed_percent = ($completed / $number_tasks) * 100;
                    $project->setAdvance($completed_percent);
                }
            }


            // Projekty zalogowanego użytkownika
            $query = $projects_repository->createQueryBuilder('p')
                ->innerJoin('p.user', 'u')
                ->where('u.id = :user_id')
                ->setParameter('user_id', $user->getId())
                ->getQuery()
                ->getResult();

            $paginator = $this->get('knp_paginator');

            $pagination = $paginator->paginate(
                $query,                                     /*query builder*/
                $request->query->get('page', 1),
                6
            );
        };

        return $this->render('project/project-list.html.twig', array(
            'projects' => $pagination,
        ));
    }


    /**
     * @Route("/project/{id}", name="project_detail")
     */
    public function showAction(Request $request, $id)
    {
        $project = null;
        $pagination = null;

        //Sprawdzenie czy użytkownik jest zalogowany
        if ($user = $this->getUser()) {

            $project = $this->getDoctrine()
                ->getRepository('AppBundle:Project')
                ->find($id);

            if (!$project){
                throw $this->createNotFoundException(
                    'Project '.$id.' does not exist'
                );
            }

            $tasks = $this->getDoctrine()
                ->getRepository('AppBundle:Task');

            // Zadania posortowanie po statusie i po nazwie
            $sorted_tasks = $tasks->createQueryBuilder('t')
                ->where('t.project = :project_id')
                ->setParameter('project_id', $project->getId())
                ->addOrderBy('t.status', 'ASC')
                ->addOrderBy('t.name', 'ASC')
                ->getQuery();

            $paginaror = $this->get('knp_paginator');
            $pagination = $paginaror->paginate(
                $sorted_tasks,
                $request->query->get('page', 1),
                10
            );
        };

        return $this->render('project/project-show.html.twig', array(
            'project' => $project,
            'tasks' => $pagination,
        ));
    }
}
