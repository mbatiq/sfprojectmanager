<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TaskRepository")
 */
class Task
{
    const STATUS_OPEN = 'otwarte';
    const STATUS_IN_PROGRESS = 'w trakcie';
    const STATUS_COMPLETED = 'zakonczone';

    const PRIORITY_LOW = 'niski';
    const PRIORITY_NORMAL = 'normalny';
    const PRIORITY_HEIGHT = 'wysoki';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="priority", type="string", length=20, options={"default" = "normalny"})
     */
    private $priority;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=20, options={"default" = "otwarte"})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="tasks")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Task
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Task
     */
    public function setPriority($priority)
    {
        if (!in_array($priority, array(
            self::PRIORITY_LOW, self::PRIORITY_NORMAL, self::PRIORITY_HEIGHT))
        ) {
            throw new \InvalidArgumentException("Invalid priority");
        }

        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority()
    {
        if (!in_array($this->priority, array(
            self::PRIORITY_LOW, self::PRIORITY_NORMAL, self::PRIORITY_HEIGHT))
        ) {
            throw new \InvalidArgumentException("Invalid priority");
        }

        return $this->priority;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     * @return Task
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Task
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Task
     */
    public function setStatus($status)
    {
        if (!in_array($status, array(
            self::STATUS_OPEN, self::STATUS_IN_PROGRESS, self::STATUS_COMPLETED))
        ) {
            throw new \InvalidArgumentException("Invalid status. You can set: ".self::STATUS_OPEN.", ".self::STATUS_IN_PROGRESS." or ".self::STATUS_COMPLETED);
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        if (!in_array($this->status, array(
            self::STATUS_OPEN, self::STATUS_IN_PROGRESS, self::STATUS_COMPLETED))
        ) {
            throw new \InvalidArgumentException("Invalid status. You can use: ".self::STATUS_OPEN.", ".self::STATUS_IN_PROGRESS." or ".self::STATUS_COMPLETED);
        }

        return $this->status;
    }
}
