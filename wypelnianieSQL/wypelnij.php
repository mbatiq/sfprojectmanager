<?php
$array_project = json_decode(file_get_contents('project.json'), true);
$array_user = json_decode(file_get_contents('user.json'), true);
$array_task = json_decode(file_get_contents('task.json'), true);

//var_dump($array_user);

$db_username = 'root';
$db_password = '';
$db_name = '';

try {
    $db = new PDO('mysql:host=localhost;dbname='.$db_name.';charset=utf8mb4', $db_username, $db_password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   //informacje o błędach przy zapytaniach do bazy
} catch(PDOException $ex) {
    echo 'Nie udało się nawiązać połączenia!', PHP_EOL;
}


//------------------------USER-------------------------

//for ($i=0; $i<5; $i++) {
//    $username = $array_user[$i]["username"];
//    $username_canonical = $array_user[$i]["username_canonical"];
//    $email = $array_user[$i]["email"];
//    $email_canonical = $array_user[$i]["email_canonical"];
//    $enabled = $array_user[$i]["enabled"];
//    $password = $array_user[$i]["password"];
//    $roles = $array_user[$i]["role"];
//    $first_name = $array_user[$i]["first_name"];
//    $last_name = $array_user[$i]["last_name"];
//
//
//    $insert_row = $db->exec("INSERT INTO user(username, username_canonical, email, email_canonical, enabled, password, roles, first_name, last_name) VALUES('".$username."', '".$username_canonical."', '".$email."', '".$email_canonical."', ".$enabled.", '".$password."', '".$roles."', '".$first_name."', '".$last_name."')");
//    $insertId = $db->lastInsertId();
//}

// --------------------PROJEKTY-------------------------

for ($i=0; $i<20; $i++) {
    $name = $array_project[$i]["name"];
    $customer = $array_project[$i]["customer"];

    $insert_row = $db->exec("INSERT INTO project(name, customer) VALUES('".$name."', '".$customer."')");
    $insertId = $db->lastInsertId();

}

// --------------Project_id User_id----------------------

$p_id = $db->query("SELECT id FROM project");
$u_id = $db->query("SELECT id FROM user");

// Utworzenie tablic z id projecktów i użytkowników
$p_array = array();
$u_array = array();

foreach ($p_id as $row) {
    array_push($p_array, $row['id']);
};

foreach ($u_id as $row) {
    array_push($u_array, $row['id']);
};

// ----------------------TASKI--------------------------

// Tablica będzie przechowywać wygenerowane pary: (projekt, użytkownik)
$project_user_array = array();

for ($i=0; $i<99; $i++) {

    // Wylosowanie po jednym elemencie z tablicy id projektów i tablicy id użytkowników
    $random_p = array_rand($p_array,1);
    $random_u = array_rand($u_array,1);

    $project_id = $p_array[$random_p];
    $user_id = $u_array[$random_u];
    $name = $array_task[$i]["name"];
    $priority = $array_task[$i]["priority"];
    $status = $array_task[$i]["status"];
    $description = $array_task[$i]["description"];

    $insert_row = $db->exec("INSERT INTO task(project_id, user_id, name, priority, description, status) VALUES(".$project_id.", ".$user_id.", '".$name."', '".$priority."', '".$description."', '".$status."')");
    $insertId = $db->lastInsertId();

    // Tworzę parę (project, user)
    $couple = ($project_id.', '.$user_id);

    // Jeśli nie ma jeszcze takiej pary, wygeneruj powiązanie: projekt użytkownik
    if (!in_array($couple, $project_user_array)) {
        $insert_row = $db->exec("INSERT INTO project_user(project_id, user_id) VALUES(" . $project_id . ", " . $user_id . ")");
        $insertId = $db->lastInsertId();
    }
    array_push($project_user_array, $couple);
}

//-----------------USER-----------------

$i = 1;
foreach ($u_array as $u) {

    $first_name = $array_user[$i]["first_name"];
    $last_name = $array_user[$i]["last_name"];

    $u_id = $db->exec("UPDATE user SET first_name='".$first_name."', last_name='".$last_name."' WHERE id=$u");
    $i++;
}

print 'Import zakończony';
?>
